from flask import Flask, redirect, url_for, render_template, request
import requests
import pandas as pd
import matplotlib as plt
import os

app = Flask(__name__)

api_key = os.environ.get('ALPHA_VANTAGE_API_KEY')

@app.route("/", methods = ['POST', 'GET'])
def main():
    if request.method == 'POST':
        user_input = request.form['nm']
        return redirect(url_for("query", ticker = user_input))
    else:
        return render_template("index.html")

@app.route("/<ticker>", methods = ['POST', 'GET'])
def query(ticker):
    search_result = None
    if request.method == 'GET':
        # replace the "demo" apikey below with your own key from https://www.alphavantage.co/support/#api-key
        symbol_url = 'https://www.alphavantage.co/query?function=OVERVIEW&symbol=' + ticker + '&apikey=' + api_key
        symbol_r = requests.get(symbol_url)
        symbol_data = symbol_r.json()

        price_url = 'https://www.alphavantage.co/query?function=GLOBAL_QUOTE&symbol=' + ticker + '&apikey=' + api_key
        price_r = requests.get(price_url)
        price_data = price_r.json()

        news_url = 'https://www.alphavantage.co/query?function=NEWS_SENTIMENT&tickers=' + ticker + '&apikey=' + api_key
        news_r = requests.get(news_url)
        news_data = news_r.json()

        plot_url = 'https://www.alphavantage.co/query?function=TIME_SERIES_DAILY_ADJUSTED&symbol=' + ticker + '&apikey=' + api_key
        plot_r = requests.get(plot_url)
        plot_data = plot_r.json()
        print(plot_data)

        plot_data = pd.DataFrame(plot_data['Time Series (Daily)'])
        plot_data = plot_data.transpose()
        plot_data = plot_data.drop(['1. open', '2. high', '3. low', '4. close',
        '6. volume', '7. dividend amount', '8. split coefficient'], axis = 1)
        plot_data = plot_data.rename(columns = {'5. adjusted close': 'AdjClose'})
        for i in range(0, len(plot_data)):
            plot_data.iloc[i, 0] = float(plot_data.iloc[i, 0])
        return render_template("result.html", symbol = symbol_data, price = price_data, news = news_data, plot = plot_data)

    else: 
        user_input = request.form['nm']
        return redirect(url_for("query", ticker = user_input))


if __name__ == "__main__":
    app.run(port=8000)
