var ready = (callback) => {
  if (document.readyState != "loading") callback();
  else document.addEventListener("DOMContentLoaded", callback);
}

ready(() => { 
    /* Do things after DOM has fully loaded */ 
  const selectElement = document.querySelector('#stock-select');
  selectElement.addEventListener('change', (event) => {
  const result = document.querySelector('.result');
  address = `static/${event.target.value}.csv`;
  title1 = $(event.target).find("option:selected").text() + " Stock Price";
  makeplot(address, title1);
});
});



function makeplot(address, title1) {
  console.log("makeplot: start")
  fetch(address)
  .then((response) => response.text()) /* asynchronous */
  .catch(error => {
      alert(error)
       })
  .then((text) => {
    console.log("csv: start")
    csv().fromString(text).then((result) => {processData(result,title1)}) /* asynchronous */
    console.log("csv: end")
  })
  console.log("makeplot: end")
};


function processData(data, title1) {
console.log("processData: start")
let x = [], y = []

for (let i=0; i<data.length; i++) {
    row = data[i];
    x.push( row['Date'] );
    y.push( row['Close'] );
} 
makePlotly( x, y , title1);
console.log("processData: end")
}

function makePlotly( x, y , title1){
console.log("makePlotly: start")
var traces = [{
      x: x,
      y: y
}];
var layout  = { title: title1}

myDiv = document.getElementById('myDiv');
Plotly.newPlot( myDiv, traces, layout );
console.log("makePlotly: end")
};