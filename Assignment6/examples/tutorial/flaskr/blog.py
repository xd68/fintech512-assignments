from flask import Blueprint
from flask import flash
from flask import g
from flask import redirect
from flask import render_template
from flask import request
from flask import url_for
from werkzeug.exceptions import abort
import os
from flaskr.auth import login_required
from flaskr.db import get_db
import requests

bp = Blueprint("blog", __name__)

api_key = os.environ.get('ALPHA_VANTAGE_API_KEY')

@bp.route("/")
def index():
    """Show all the posts, most recent first."""
    db = get_db()
    calculate = db.execute(
        "SELECT p.ticker, p.tracking_price, p.id"
        " FROM post p"
        " ORDER BY p.created DESC"
    ).fetchall()
    for i in calculate:
        this_ticker = i[0]
        this_tracking_price = float(i[1])
        this_id = i[2]
        price_url = 'https://www.alphavantage.co/query?function=GLOBAL_QUOTE&symbol=' + this_ticker + '&apikey=OLLQRX6EX4RQECTT'
        price_r = requests.get(price_url)
        price_data = price_r.json()
        curr_price_1 = float((price_data['Global Quote']['05. price']))
        curr_pct_1 = (curr_price_1 - this_tracking_price)/this_tracking_price
        db.execute(
            "UPDATE post SET curr_price = ?, curr_pct = ? WHERE id = ?",
            (curr_price_1, curr_pct_1, this_id)
        )
        db.commit()


    posts = db.execute(
        "SELECT p.id, p.ticker, p.tracking_price, p.num_shares, p.created, p.author_id, u.username, p.curr_price, p.curr_pct"
        " FROM post p JOIN user u ON p.author_id = u.id"
        " ORDER BY created DESC"
    ).fetchall()
    return render_template("blog/index.html", posts=posts)


def get_post(id, check_author=True):
    """Get a post and its author by id.

    Checks that the id exists and optionally that the current user is
    the author.

    :param id: id of post to get
    :param check_author: require the current user to be the author
    :return: the post with author information
    :raise 404: if a post with the given id doesn't exist
    :raise 403: if the current user isn't the author
    """
    post = (
        get_db()
        .execute(
            "SELECT p.id, ticker, tracking_price, num_shares, created, author_id, username"
            " FROM post p JOIN user u ON p.author_id = u.id"
            " WHERE p.id = ?",
            (id,),
        )
        .fetchone()
    )

    if post is None:
        abort(404, f"Post id {id} doesn't exist.")

    if check_author and post["author_id"] != g.user["id"]:
        abort(403)

    return post


@bp.route("/create", methods=("GET", "POST"))
@login_required
def create():
    """Create a new post for the current user."""
    if request.method == "POST":
        ticker = request.form["ticker"]
        tracking_price = request.form["tracking_price"]
        num_shares = request.form["num_shares"]
        error = None

        if not ticker:
            error = "Ticker is required."

        if error is not None:
            flash(error)
        else:
            db = get_db()
            db.execute(
                "INSERT INTO post (ticker, tracking_price, num_shares, author_id) VALUES (?, ?, ?, ?)",
                (ticker, tracking_price, num_shares, g.user["id"]),
            )
            db.commit()
            return redirect(url_for("blog.index"))

    return render_template("blog/create.html")





@bp.route("/<int:id>/update", methods=("GET", "POST"))
@login_required
def update(id):
    """Update a post if the current user is the author."""
    post = get_post(id)

    if request.method == "POST":
        ticker = request.form["ticker"]
        tracking_price = request.form["tracking_price"]
        num_shares = request.form["num_shares"]

        error = None

        if not ticker:
            error = "Ticker is required."

        if error is not None:
            flash(error)
        else:
            db = get_db()
            db.execute(
                "UPDATE post SET ticker = ?, tracking_price = ? , num_shares = ? WHERE id = ?", (ticker, tracking_price, num_shares, id)
            )
            db.commit()
            return redirect(url_for("blog.index"))

    return render_template("blog/update.html", post=post)


@bp.route("/<int:id>/delete", methods=("POST", "GET"))
@login_required
def delete(id):
    """Delete a post.

    Ensures that the post exists and that the logged in user is the
    author of the post.
    """
    get_post(id)
    if request.method == "POST":
        db = get_db()
        db.execute("DELETE FROM post WHERE id = ?", (id,))
        db.commit()
        return redirect(url_for("blog.index"))

'''
@bp.route("/<int:id>/delete", methods=("POST",))
@login_required
def delete1(id):
    """Delete a post.

    Ensures that the post exists and that the logged in user is the
    author of the post.
    """

    get_post(id)
    if request.method == 'POST':
        db = get_db()
        db.execute("DELETE FROM post WHERE id = ?", (id,))
        flash("Post deleted successfully.")
        db.commit()
        return redirect(url_for("blog.index"))
'''