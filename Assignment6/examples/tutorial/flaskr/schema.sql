-- Initialize the database.
-- Drop any existing data and create empty tables.

DROP TABLE IF EXISTS user;
DROP TABLE IF EXISTS post;

CREATE TABLE user (
  id INTEGER PRIMARY KEY AUTOINCREMENT,
  username TEXT UNIQUE NOT NULL,
  password TEXT NOT NULL
);

CREATE TABLE post (
  id INTEGER PRIMARY KEY AUTOINCREMENT,
  author_id INTEGER NOT NULL,
  created TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  ticker TEXT NOT NULL,
  tracking_price NUMERIC(10, 2) NOT NULL,
  num_shares DECIMAL NOT NULL,
  curr_price NUMERIC(10, 2) NOT NULL DEFAULT 0,
  curr_pct NUMERIC(10, 2) NOT NULL DEFAULT 0,

  FOREIGN KEY (author_id) REFERENCES user (id)
);
